﻿using Amazon.DynamoDBv2.DataModel;

namespace DynamoDBQueryConsole.Model
{
    //This Model acts as a mapping between the DynamoDB table's documents we're interested in working with
    //and translates into an equivalent representation of the C# class, thereby enabling us to work with the DynamoDB data types directly.
    [DynamoDBTable("Customer")] //to map the DynamoDB equivalent table
    public class Customer
    {
        [DynamoDBHashKey] //indicates the field which acts as the PrimaryKey
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
