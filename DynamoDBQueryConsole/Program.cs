﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

using DynamoDBQueryConsole.Model;

namespace DynamoDBQueryConsole
{
    class Program
    {
        private static AmazonDynamoDBClient dBClient = new AmazonDynamoDBClient("AKIARKIFTSUYYHNFILZP", "rBbpHQAOt692hQnb8Xx+67URVFr8628dL2+k/lPW", RegionEndpoint.USEast2);
        private static DynamoDBOperationConfig dBConfig = new DynamoDBOperationConfig();
        static void Main(string[] args)
        {
            DynamoDBContext dynamoDBContext = new DynamoDBContext(dBClient);

            dBConfig.OverrideTableName = "Customer";
            dBConfig.IndexName = "CustomerId-index";

            //Create and save new customer record
            Customer newCust = new Customer
            {
                CustomerId = "306E7297-5529-4CFC-9009-F8933D774EC1",
                FirstName = "Little",
                LastName = "Rock"
            };

            //var saveCust =  SaveCustomerAsync(dynamoDBContext, newCust);

            // Get single Customer record
            string id = "306E7297-5529-4CFC-9009-F8933D774EC1";
            var customerRecord = GetCustomerByIdAsync(dynamoDBContext, id).GetAwaiter().GetResult();
            foreach (var cust in customerRecord)
            {
                Console.WriteLine(cust.FirstName + " " + cust.LastName);
            }

            //Get all customers
            var searchResults = GetCustomerAll(dynamoDBContext).Result;
            foreach (var r in searchResults)
            {
                Console.WriteLine("Id:{0}\t Firstname:{1}\t Lastname:{2}", r.CustomerId, r.FirstName, r.LastName);
            }
        }

        public static async Task<IEnumerable<Customer>> GetCustomerByIdAsync(DynamoDBContext dBContext, string id)
        {
            var scanConditions = new List<ScanCondition>();
            if (!string.IsNullOrEmpty(id))
                scanConditions.Add(new ScanCondition("CustomerId", ScanOperator.Equal, id));
            var test = await dBContext.ScanAsync<Customer>(scanConditions, null).GetRemainingAsync();
            return test;

        }

        public static async Task<List<Customer>> GetCustomerAll(DynamoDBContext dBContext)
        {
            /* The Non-Pagination approach */
            var scanConditions = new List<ScanCondition>()
            {
            new ScanCondition("CustomerId", ScanOperator.IsNotNull)
            };
            var searchResults = dBContext.ScanAsync<Customer>(scanConditions, null);
            return await searchResults.GetNextSetAsync();

        }

        public static async Task SaveCustomerAsync(DynamoDBContext dBContext, Customer customer)
        {
            //this method invocation will override existing matched record
            await dBContext.SaveAsync(customer, dBConfig);
        }
    }
}
